﻿using System;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;

namespace AD_auth
{
    class AD_Authenticationcs
    {
        private DirectorySearcher dirSearch = null;
        private string userDomanin;
        private string ldapServerPort;

        public AD_Authenticationcs( string domain,string portno)
        {
            this.userDomanin = domain;
            this.ldapServerPort = portno;
        }
        public AD_Authenticationcs(string domain)
        {
            this.userDomanin = domain;
            
        }
        
        private bool CheckConnectionToLDAP(string ldapserverAdress, string portNo)
        {
            
            LdapConnection ldapConnection = new LdapConnection(new LdapDirectoryIdentifier((ldapserverAdress + (":" + portNo))));
            TimeSpan myTimeout = new TimeSpan(0, 0, 0, 1);
            try
            {
                ldapConnection.AuthType = AuthType.Anonymous;
                ldapConnection.AutoBind = false;
                ldapConnection.Timeout = myTimeout;
                ldapConnection.Bind();
                Console.WriteLine(("Successfully authenticated to LDAP server: " + ldapserverAdress));
                ldapConnection.Dispose();
                return true;
            }
            catch (LdapException e)
            {
                Console.WriteLine(("Error to connect with LDAP Server: " + ldapserverAdress));
                Console.WriteLine((e.ErrorCode + (":" + e.Message)));
                return false;
            }
        }

        private DirectorySearcher CheckUserAccount(string userName, string userPass)
        {
            if (dirSearch == null)
            {
                try
                {
                    dirSearch = new DirectorySearcher(
                        new DirectoryEntry("LDAP://" + userDomanin, userName, userPass));
                }
                catch (DirectoryServicesCOMException e)
                {
                    Console.WriteLine("Connection Creditial is Wrong!!!, please Check.");
                    Console.WriteLine((e.ErrorCode + (":" + e.Message)));
                }
                return dirSearch;
            }
            else
            {
                return dirSearch;
            }
        }
        private SearchResult SearchUserByUserName(DirectorySearcher ds, string username)
        {
            ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + username + "))";

            ds.SearchScope = System.DirectoryServices.SearchScope.Subtree;
            ds.ServerTimeLimit = TimeSpan.FromSeconds(90);

            SearchResult userObject = ds.FindOne();

            if (userObject != null)
            {
                Console.Write("Welcome: " + userObject.GetDirectoryEntry().Properties["givenName"].Value.ToString());
                Console.Write(" "+userObject.GetDirectoryEntry().Properties["sn"].Value.ToString());
                return userObject;
            }
            else
            {
                Console.WriteLine("User:{0} not found",username);
                return null;
            }
                
        }
    }
}
